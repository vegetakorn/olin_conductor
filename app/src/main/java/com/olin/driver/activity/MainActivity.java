package com.olin.driver.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pkmmte.view.CircularImageView;
import com.olin.driver.R;
import com.olin.driver.adapter.NavigationAdapter;
import com.olin.driver.data.DrawerData;
import com.olin.driver.data.ProfileData;
import com.olin.driver.data.RideDetails;
import com.olin.driver.fragment.ArrivalBuzzFragment;
import com.olin.driver.fragment.ChangePasswordFragment;
import com.olin.driver.fragment.DriverDetailsFragment;
import com.olin.driver.fragment.HomeFragment;
import com.olin.driver.fragment.MyReservationsFragment;
import com.olin.driver.fragment.RideRequestFragment;
import com.olin.driver.fragment.SettingsFragment;
import com.olin.driver.fragment.StartRideFragment;
import com.olin.driver.service.LocationUpdateService;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import me.tangke.slidemenu.SlideMenu;

/**
 * Created by neeraj.narwal on 5/7/16.
 */
public class MainActivity extends BaseActivity implements BaseActivity.PermCallback, ImageUtils.ImageSelectCallback {

    private static final int AVAILABLE = 1;
    private AlertDialog dialog;
    private HomeFragment homeFragment;
    private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Const.DISPLAY_MESSAGE_ACTION)) {
                Bundle pushData = intent.getBundleExtra("map");

                String action = (String) pushData.get("action");
                String controller = (String) pushData.get("controller");
                log(action + " " + controller);
                if (action != null && controller != null) {
                    if (action.equalsIgnoreCase("request") && controller.equalsIgnoreCase("ride")) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        try {
                            gotoRideRequestFragment(Integer.parseInt(pushData.getString("id")));
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (action.equalsIgnoreCase("accepted")) {


                    } else if (action.equalsIgnoreCase("arrived")) {

                    } else if (action.equalsIgnoreCase("started")) {

                    } else if (action.equalsIgnoreCase("canceled")) {
                        gotoHomeFragment();
                    } else if (action.equalsIgnoreCase("paymentReceived")) {

                    } else if (action.equalsIgnoreCase("payment")) {

                    } else if (controller.equalsIgnoreCase("ride") && action.equalsIgnoreCase("wallet-pay")) {
                        showPendingAmountDialog((String) pushData.get("message"));
                        cancelNotification(Const.ALL_NOTI);

                    } else if (controller.equalsIgnoreCase("ride") && action.equalsIgnoreCase("update")) {
                        try {
                            getJourneyDetailSingleShot(pushData.getString("id"));
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (action.equalsIgnoreCase("cash-pay") || action.equalsIgnoreCase("paypal-pay")) {
                        showRidePaidDialog();
                        cancelNotification(Const.ALL_NOTI);
                    } else if (action.equalsIgnoreCase("add") && controller.equalsIgnoreCase("complain")) {
                        showComplainDialog();
                        cancelNotification(Const.ALL_NOTI);
                    }

                }
            }
        }
    };


    private void showComplainDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning !")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(this.getString(R.string.warning_message));
        builder.create().show();
    }

    private void showRidePaidDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.attention))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.to_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.payment_done));
        builder.create().show();
    }

    private void showPendingAmountDialog(String left_amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.attention))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.to_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .setMessage(getString(R.string.you_have) + " " + left_amount + " " + getString(R.string.to_collect_from_passenger));
        builder.create().show();

    }

    private long times_in_millies = 60 * 1000 * 5;// 5 minutes timer
    private CountDownTimer cTimer;
    private long remianing_times_in_millies;
    private FrameLayout container;
    private ListView drawerLV;
    public CircularImageView profileCIV;
    private TextView userNameTV;
    private DrawerData navData;
    private LinearLayout headerRL;
    private CheckBox availableBusySW;
    private TextView ratingTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if (getIntent().hasExtra("rideDetail")) {
            RideDetails rideDetail = getIntent().getParcelableExtra("rideDetail");
            gotoCurrentState(rideDetail);
        } else if (getIntent().hasExtra("map")) {
            Bundle bundle;
            bundle = getIntent().getBundleExtra("map");
            if (bundle.getString("controller").equalsIgnoreCase("complain")
                    && bundle.getString("action").equalsIgnoreCase("add")) {
                showComplainDialog();
                cancelNotification(Const.ALL_NOTI);
            }
            try {
                getJourneyDetailSingleShot(bundle.getString("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            gotoCurrentState(null);

        }
        hideSoftKeyboard();
        cancelNotification(Const.ALL_NOTI);
    }


    private void gotoRideRequestFragment(int id) {
        Fragment fragment = new RideRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    private void init() {
        availableBusySW = (CheckBox) findViewById(R.id.availableBusySW);
        container = (FrameLayout) findViewById(R.id.container);
        slideMenu = (SlideMenu) findViewById(R.id.slideMenu);
        profileCIV = (CircularImageView) findViewById(R.id.profileCIV);
        userNameTV = (TextView) findViewById(R.id.userNameTV);
        ratingTV = (TextView) findViewById(R.id.ratingTV);
        headerRL = findViewById(R.id.profileLL);
        slideMenu.setEdgeSlideEnable(false);
        slideMenu.setEdgetSlideWidth(0);
        headerRL.setOnClickListener(this);
        profileCIV.setOnClickListener(this);
        slideMenu.setPrimaryShadowWidth(0);
        setToolbar("");
        updateDrawer();
        setIsAvailable();

    }

    private void setIsAvailable() {
        try {
            String is_available = store.getString(Const.IS_AVAILABLE);
            log("is_available== " + is_available);
            if (is_available != null && !is_available.isEmpty()) {
                if (is_available.equals("0")) {
                    availableBusySW.setChecked(false);
                } else {
                    availableBusySW.setChecked(true);
                    setDriverStatus();
                }
            } else {
                availableBusySW.setChecked(false);
            }
            availableBusySW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    setDriverStatus();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void setDriverStatus() {
        int is_available;
        if (availableBusySW.isChecked()) {
            is_available = 1;
        } else is_available = 0;
        RequestParams params = new RequestParams();
        params.put("User[is_online]", is_available);
        syncManager.sendToServer("user/status", params, this);
    }

    public void setDriverStatus(int is_available) {
        RequestParams params = new RequestParams();
        params.put("User[is_online]", is_available);
        syncManager.sendToServer("user/status", params, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == 0) {
            if (Build.VERSION_CODES.M == android.os.Build.VERSION.SDK_INT) {
                if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0, this)) {
                }
            } else {

            }
        } else {
            showToast(getString(R.string.play_services_not_updated));
        }

    }

    @Override
    protected void onResume() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.DISPLAY_MESSAGE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHandleMessageReceiver, intentFilter);
        //LocationUpdateService.startService(MainActivity.this);
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mHandleMessageReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandleMessageReceiver);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.profileLL:
                slideMenu.close(true);
                break;

            case R.id.profileCIV:
                slideMenu.close(true);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new DriverDetailsFragment()).addToBackStack(null).commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof HomeFragment) {
            back();
        } else if (
                fragment instanceof SettingsFragment
                        || fragment instanceof MyReservationsFragment
                        || fragment instanceof DriverDetailsFragment
                        || fragment instanceof ChangePasswordFragment
                ) {
            getSupportFragmentManager().popBackStack();

        }
        hideSoftKeyboard();

    }

    public void setToolbar(String title) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        toolbar_imageIV = (ImageView) findViewById(R.id.toolbar_imageIV);
        setSupportActionBar(toolbar);
        getSupportActionBar().show();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_titleTV.setText(title);
        if (fragment instanceof ChangePasswordFragment
                || fragment instanceof RideRequestFragment
                || fragment instanceof DriverDetailsFragment
                || fragment instanceof StartRideFragment
                || fragment instanceof ArrivalBuzzFragment
                || fragment instanceof SettingsFragment
                || fragment instanceof MyReservationsFragment
                ) {
            toolbar.setNavigationIcon(null);
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_black);
        } else {
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_drawer);
        }
        toolbar.setVisibility(View.VISIBLE);
        toolbar_titleTV.setVisibility(View.VISIBLE);
        toolbar_titleTV.setTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof ChangePasswordFragment
                        || fragment instanceof RideRequestFragment
                        || fragment instanceof DriverDetailsFragment
                        || fragment instanceof StartRideFragment
                        || fragment instanceof SettingsFragment
                        || fragment instanceof ArrivalBuzzFragment
                        || fragment instanceof MyReservationsFragment
                        ) {
                    onBackPressed();
                } else {
                    slideMenu.open(false, true);
                }
            }
        });
    }


    private void gotoHomeFragment() {
        if(homeFragment == null){
            homeFragment = new HomeFragment();
        }
        if(getSupportFragmentManager().getFragments().contains(homeFragment)){
            getSupportFragmentManager().getFragments().remove(homeFragment);
            homeFragment = new HomeFragment();
        }
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, homeFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    @SuppressLint("SetTextI18n")
    public void updateDrawer() {
        ArrayList<DrawerData> navDrawerItems = new ArrayList<>();
        drawerLV = (ListView) findViewById(R.id.drawerLV);
        String[] navigationMenu = getResources().getStringArray(R.array.drawer_item_string);
        int[] navDrawerImage = {R.mipmap.ic_drawer_journey, R.mipmap.ic_drawer_settings, R.mipmap.ic_drawer_logout};
        for (int i = 0; i < navigationMenu.length; i++) {
            navData = new DrawerData();
            navData.name = navigationMenu[i];
            navData.icon = navDrawerImage[i];
            navDrawerItems.add(navData);
        }

        NavigationAdapter adapter = new NavigationAdapter(this, navDrawerItems);
        drawerLV.setAdapter(adapter);
        drawerLV.setOnItemClickListener(seekerDrawerListener);
        ProfileData profileData = getProfileDataFromPrefStore();
        try {
            picasso.load(profileData.image_file).error(R.mipmap.default_avatar).into(profileCIV);
        } catch (Exception e) {
            e.printStackTrace();
        }
        userNameTV.setText(profileData.first_name + " " + profileData.last_name);
        if (profileData.rating != null)
            ratingTV.setText("" + round(Float.valueOf(profileData.rating), 1));
        else
            ratingTV.setText("0");

    }

    AdapterView.OnItemClickListener seekerDrawerListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment frag = null;
            switch (position) {
                case 0://myreservasions
                    frag = new MyReservationsFragment();
                    break;
                case 1:
                    //settings
                    frag = new SettingsFragment();
                    break;
                case 2: // logout
                    logout();
                    break;


            }
            slideMenu.close(true);
            if (frag != null)
                getSupportFragmentManager().beginTransaction().replace(R.id.container, frag).addToBackStack(null).commit();
        }
    };

    private void logout() {
        cancelNotification(Const.ALL_NOTI);
        syncManager.sendToServer("user/logout", null, this);
    }

    @Override
    public void permGranted(int resultCode) {

    }

    @Override
    public void permDenied(int resultCode) {
    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
    }

    void startTimer() {
        cTimer = new CountDownTimer(times_in_millies, 1000) {
            public void onTick(long millisUntilFinished) {
                remianing_times_in_millies = millisUntilFinished;
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;                /* minutesTV.setText(String.format("%02d", minutes)                         + " m " + String.format("%02d", seconds) + " s");*/
            }

            public void onFinish() {         /* minutesTV.setText("00 m 00 s);*/
                cTimer = null;
            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if (cTimer != null) cTimer.cancel();
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (action.equals("logout")) {
            if (status) {
                syncManager.setLoginStatus(null);
                startActivity(new Intent(this, LoginSignupActivity.class));
                finish();
                LocationUpdateService.stopService(this);
            }
        } else if (controller.equals("user") && action.equals("status")) {
            if (status) {
                String state  = jsonObject.optString("state");
                if(state.equals("0")){
                    LocationUpdateService.stopService(this);
                }else{
                    LocationUpdateService.startService(this);
                }
                store.saveString(Const.IS_AVAILABLE, state);
                showToast(jsonObject.optString("detail"));
            } else {
                availableBusySW.setChecked(false);
                if (jsonObject.has("error"))
                    showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                JSONObject detail = jsonObject.optJSONObject("detail");
                RideDetails rideDetails = parseRideDetails(detail);
                gotoCurrentState(rideDetails);
            } else {
                log(jsonObject.optString("error"));
            }
        }
    }




    public void gotoCurrentState(RideDetails journeyDetail) {
        if (journeyDetail != null) {
            switch (journeyDetail.state_id) {
                case Const.STATE_NEW:
                    gotoRideRequestFragment(journeyDetail.id);
                    break;
                case Const.STATE_ACCEPTED:
                    gotoArrivalBuzzFragment(journeyDetail.id);
                    break;
                case Const.STATE_DRIVER_ARRIVED:
                    gotoStartRideFragment(journeyDetail.id);
                    break;
                case Const.STATE_STARTED:
                    gotoStartRideFragment(journeyDetail.id);
                    break;
                case Const.STATE_COMPLETED:
                    gotoHomeFragment();
                    break;
                case Const.STATE_CANCELLED:
                    gotoHomeFragment();
                    log("My log journey cancelled by passenger");
                    break;
                default:
                    gotoHomeFragment();
            }
        } else {
            gotoHomeFragment();
        }
        }


    private void showJourneyCancelledDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.ride_cancelled));
        builder.setMessage(getString(R.string.ride_has_been_cancelled));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogg) {
                if (dialog != null && !MainActivity.this.isFinishing() && dialog.isShowing())
                    dialog.dismiss();
                gotoHomeFragment();
            }
        });
        dialog = builder.create();
        if (dialog != null && !this.isFinishing() && !dialog.isShowing())
            dialog.show();
    }

    public void getJourneyDetailSingleShot(String ride_id) {

        syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);

    }

    private void gotoStartRideFragment(int id) {
        Fragment fragment = new StartRideFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }

    private void gotoArrivalBuzzFragment(int id) {
        Fragment fragment = new ArrivalBuzzFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }
}
