package com.olin.driver.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olin.driver.R;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */

public class RideDetailFragment extends BaseFragment {
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ride_detail, container, false);
        baseActivity.setActionBarTitle("Ride Details", true);
        baseActivity.toolbar_titleTV.setAllCaps(false);
        return view;
    }

}
