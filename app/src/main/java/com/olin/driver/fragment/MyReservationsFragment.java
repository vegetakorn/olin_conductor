package com.olin.driver.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.olin.driver.R;
import com.olin.driver.activity.MainActivity;
import com.olin.driver.adapter.ReservationsAdapter;
import com.olin.driver.data.RideDetails;
import com.olin.driver.utils.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by TOXSL\chirag.tyagi on 17/3/17.
 */

public class MyReservationsFragment extends BaseFragment {
    private View view;
    private TextView emptyTV;
    private ListView reservationsLV;
    private ReservationsAdapter adapter;
    private int currentPage = 0;
    private int pageCount = 1;
    private ArrayList<RideDetails> rides = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_reservations, container, false);
        ((MainActivity) baseActivity).setToolbar(getString(R.string.my_reservations));
        initUI();
        hitRideHistoryApi();
        return view;
    }

    public void hitRideHistoryApi() {
        if (currentPage + 1 <= pageCount)
            baseActivity.syncManager.sendToServer(Const.API_RIDE_DRIVER_RIDE_HISTORY + "?page=" + currentPage, null, this);
    }

    private void initUI() {
        emptyTV = (TextView) view.findViewById(R.id.emptyTV);
        reservationsLV = (ListView) view.findViewById(R.id.reservationsLV);
        reservationsLV.setEmptyView(emptyTV);
        adapter = new ReservationsAdapter(baseActivity, rides, this);
        reservationsLV.setAdapter(adapter);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("ride") && action.equalsIgnoreCase("driver-ride-history")) {
            if (status) {
                currentPage++;
                if (jsonObject.has("totalPage")) {
                    pageCount = jsonObject.optInt("totalPage");
                }
                JSONArray list = jsonObject.optJSONArray("list");
                for (int i = 0; i < list.length(); i++) {
                    RideDetails rideDetail = baseActivity.parseRideDetails(list.optJSONObject(i));
                    rides.add(rideDetail);
                }
                adapter.notifyDataSetChanged();
            } else {
                showToast(jsonObject.optString("error"));
            }
        }
    }


}
