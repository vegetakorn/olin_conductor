package com.olin.driver.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import android.provider.Settings;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.LocationSource;
import com.olin.driver.BuildConfig;
import com.olin.driver.R;
import com.olin.driver.activity.MainActivity;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.PrefStore;
import com.olin.driver.utils.Util;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.RequestParams;
import com.toxsl.volley.toolbox.SyncEventListner;
import com.toxsl.volley.toolbox.SyncManager;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class LocationUpdateService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SyncEventListner, SensorEventListener, LocationSource {
    private static final String TAG = "LocationUpdateService";
    private static final long INTERVAL = 1000 * 30; // 1 minuto
    private static LocationUpdateService instance;

    private SyncManager syncManager;
    private static Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String mLastUpdateTime;
    private boolean notifyVisible;
    private static OnLocationReceived mLocationReceived;
    private PrefStore store;
    private final String CHANNEL = "driverchannel";
    // This is any integer value unique to the application.
    public final int SERVICE_RUNNING_NOTIFICATION_ID = 52847;
    private IBinder binder;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    //Con este es que funciona
    private LocationListener listener;
    private LocationManager locationManager;


    public static LocationUpdateService getInstance(Context mAct) {
        mContext = mAct;
        if (instance == null) {
            instance = new LocationUpdateService();
        }
        return instance;
    }

    /**
     * Inicial el servicio de monitoreo
     */
    public static void startService(Context context) {
        //No se inicia el servicio 2 veces
        if(Util.isMyServiceRunning(LocationUpdateService.class,context)){
            return;
        }
        Intent intent = new Intent(context, LocationUpdateService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            context.startForegroundService(intent);
        }
        else
        {
            context.startService(intent);
        }
    }

    public static void stopService(Context context) {
        Intent callIntent = new Intent(context, LocationUpdateService.class);
        context.stopService(callIntent);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    public void setLocationReceivedListner(OnLocationReceived mLocationReceived) {
        LocationUpdateService.mLocationReceived = mLocationReceived;
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {

    }

    @Override
    public void deactivate() {

    }

    public interface OnLocationReceived {

        void onLocationReceived(Location location);

    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Util.log(TAG,"onStartCommand");
        super.onStartCommand(intent, flags, startId);
        if (mContext == null)
            mContext = this;



        store = new PrefStore(this);
        syncManager = SyncManager.getInstance(this, BuildConfig.DEBUG);
        syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name), store.getString("language", "es"));
        init();
        notifyVisible = false;

        //Notificacion persistente
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL);

        builder
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(getResources().getString(R.string.notification_service_text))
                .setSmallIcon(R.mipmap.notification_1)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        //StopSelf();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            // Notification channels are new in API 26 (and not a part of the
            // support library). There is no need to create a notification
            // channel on older versions of Android.

            NotificationManager notManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            NotificationChannel chan = new NotificationChannel(CHANNEL, "Notification", NotificationManager.IMPORTANCE_HIGH);
            chan.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            notManager.createNotificationChannel(chan);
            builder.setChannelId(CHANNEL);

        }
        Notification notification = builder.build();


        // Enlist this instance of the service as a foreground service
        startForeground(SERVICE_RUNNING_NOTIFICATION_ID, notification);

        LocationUpdateService locationUpdateService = this;
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Util.log(TAG,"demo onLocationChanged "+location.getLongitude()+" "+location.getLatitude());
                //Intent i = new Intent("location_update");
                //i.putExtra("coordinates",location.getLongitude()+" "+location.getLatitude());
                //sendBroadcast(i);
                locationUpdateService.onLocationChanged(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3000,0,listener);
        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,3000,0,listener);

        return Service.START_STICKY_COMPATIBILITY;
    }

    private void init() {
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        stopLocationUpdates();

        Util.log(TAG, "ondestroy!");
        stoptimertask();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Util.log(TAG,"onCreate");
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    long counter=0;
    private void startTimer() {
        stoptimertask();
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //Correr la primera vez
        timerTask.run();
        //schedule the timer, to wake up every 30 second
        timer.schedule(timerTask, 30000, 10000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Util.log(TAG, "in timer ++++  "+ (counter++));
                setCurrentLocation();
            }
        };
    }
    private void setCurrentLocation() {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Util.log(TAG, "No se tiene los permisos para obtener la ubicacion");
                return;
            }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                    sendLocationUpdate(location);
                    mFusedLocationClient.flushLocations();
            }else{
                Util.log(TAG,"location is null");
            }
        });
    }

    private void sendLocationUpdate(Location location) {
        RequestParams params = new RequestParams();
        Util.log(TAG,location.getLatitude() + "  " + location.getLongitude());
        params.put("User[lat]", location.getLatitude());
        params.put("User[long]", location.getLongitude());
        params.put("User[direction]", "0.0");
        syncManager.sendToServer(Const.API_USER_SET_LOCATION, params, this);
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Util.log(TAG,"onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        try {
            startLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            generateNotification(1);
            stopService(mContext);
            return;
        }

        //if(mFusedLocationClient == null){
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(INTERVAL);
            mLocationRequest.setMaxWaitTime(60 * 60 * 1000);
            //mLocationRequest.setSmallestDisplacement(10.0f);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            locationCallback = new LocationCallback(){
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    Util.log(TAG,"onLocationResult "+locationResult);
                    if(locationResult == null){
                        return;
                    }
                    onLocationChanged(locationResult.getLastLocation());
                }

                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    super.onLocationAvailability(locationAvailability);
                }
            };

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            //mFusedLocationClient.requestLocationUpdates(mLocationRequest,locationCallback, Looper.getMainLooper());
            Util.log(TAG,"Location update started ..............: ");
        //}
        /*
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        */

        startTimer();
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()){
            //LocationServices.FusedLocationApi.removeLocationUpdates(
            //        mGoogleApiClient, this);

            if(mFusedLocationClient != null && locationCallback != null){
                mFusedLocationClient.removeLocationUpdates(locationCallback);
                mFusedLocationClient = null;
                locationCallback = null;
            }
        }

        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
        Util.log(TAG,"Location update stopped .......................");
    }


    private boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        Boolean enable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (enable) {
            notifyVisible = false;
        }
        return enable;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Util.log(TAG,"Connection failed: " + connectionResult.toString());
    }

    public void onLocationChanged(Location location) {
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        Util.log(TAG,"Firing onLocationChanged................    latitude=  " + location.getLatitude() + "    longitude=   " + location.getLongitude() + "  " + mLastUpdateTime);
        handleNotifications();
        RequestParams params = new RequestParams();
        Util.log(TAG,location.getLatitude() + "  " + location.getLongitude());
        params.put("User[lat]", location.getLatitude());
        params.put("User[long]", location.getLongitude());
        params.put("User[direction]", 0.0);
        syncManager.sendToServer(Const.API_USER_SET_LOCATION, params, this);
        if (mLocationReceived != null)
            mLocationReceived.onLocationReceived(location);
    }

    public void handleNotifications() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            generateNotification(1);
        } else if (!isGPSEnabled()) {
            generateNotification(2);
        }
    }

    private void generateNotification(int i) {
        if (!notifyVisible) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "tsd_01";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications",NotificationManager.IMPORTANCE_HIGH);
                // Configure the notification channel.
                notificationChannel.setDescription("Ubicacion");
                notificationManager.createNotificationChannel(notificationChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext,NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.notification_1)
                    .setAutoCancel(true);
            Intent mainIntent;
            String message;
            if (i == 1) {
                mBuilder.setContentTitle(mContext.getString(R.string.app_name));
                message = mContext.getString(R.string.please_relaunch) + mContext.getString(R.string.app_name);
            } else {
                mBuilder.setContentTitle(mContext.getString(R.string.app_name) + mContext.getString(R.string.gps));
                message = mContext.getString(R.string.check_gps);
                mainIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            }
            mBuilder.setContentText(message);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            Uri NotiSound = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(NotiSound);
            long[] vibrate = {600, 100, 100, 700};
            mBuilder.setVibrate(vibrate);
//            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 12, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) mContext
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(i, mBuilder.build());
            notifyVisible = true;
        }
    }



    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncFinish() {

    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        Util.log(TAG,"Service onSyncSuccess--------" + action + "====" + status);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > INTERVAL;
        boolean isSignificantlyOlder = timeDelta < -INTERVAL;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public FusedLocationProviderClient getFusedLocationClient(){
        if(mFusedLocationClient == null){
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        }
        return mFusedLocationClient;
    }

}
