package com.olin.driver.fragment;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.olin.driver.R;
import com.olin.driver.activity.BaseActivity;
import com.olin.driver.data.RideDetails;
import com.olin.driver.service.LocationUpdateService;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.DataUtil;
import com.olin.driver.utils.PrefStore;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.SyncEventListner;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TOXSL\neeraj.narwal on 2/2/16.
 */
public class BaseFragment extends Fragment implements AdapterView.OnItemClickListener,
        View.OnClickListener, SyncEventListner, AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener, LocationListener, LocationUpdateService.OnLocationReceived {
    public BaseActivity baseActivity;
    public LocationUpdateService locationUpdateService;
    public Animation startAnim;
    public Animation exitAnim;
    protected JourneyDetailListener journeyDetailListener;
    private long timespan;
    PrefStore store;
    private boolean bPaused;
    private DataUtil dataUtil;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (!BaseFragment.this.bPaused) {
                log("basefrag runnable get journeydetailsingleshot");
                getJourneyDetailSingleShot();
                handler.postDelayed(runnable, timespan);
            } else
                log("bpaused true");
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
        store = new PrefStore(baseActivity);
        locationUpdateService = LocationUpdateService.getInstance(baseActivity);
        startAnim = AnimationUtils.loadAnimation(getContext(), R.anim.popup_show);
        exitAnim = AnimationUtils.loadAnimation(getContext(), R.anim.popup_hide);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        baseActivity.checkPlayServices();
        baseActivity.hideSoftKeyboard();
        getActivity().invalidateOptionsMenu();
    }


    @Override
    public void onClick(View v) {

    }

    public void getJourneyDetailSingleShot() {
        int journeyId = store.getInt(Const.JOURNEY_ID);
        if (journeyId != 0) {
            baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + journeyId, null, this);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                baseActivity.getSupportFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showToast(String msg) {
        baseActivity.showToast(msg);
    }

    @Override
    public void onSyncStart() {
        baseActivity.onSyncStart();
    }

    @Override
    public void onSyncFinish() {
        baseActivity.onSyncFinish();
    }

    @Override
    public void onSyncFailure(VolleyError code, Request mRequest) {
        baseActivity.onSyncFailure(code, mRequest);
    }

    public void log(String s) {
        baseActivity.log(s);
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public interface JourneyDetailListener {
        void updateDetail(RideDetails journeyDetail);
    }

    public void setJourneyListener(JourneyDetailListener journeyDetailListener) {
        log("set journey listner: " + journeyDetailListener);
        this.journeyDetailListener = journeyDetailListener;
    }

    public void getJourneyDetail(long timeSpanInMillis) {
        timespan = timeSpanInMillis;
        if (handler == null)
            handler = new Handler();
        handler.sendEmptyMessage(1);
        handler.post(runnable);
    }


    public void startAnimation(final LinearLayout view, final boolean isStart) {
        Animation animation;
        if (isStart) {
            animation = startAnim;
        } else {
            animation = exitAnim;
        }
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isStart)
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.clearAnimation();
        if (isStart) {
            view.startAnimation(startAnim);
        } else {
            view.startAnimation(exitAnim);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public void gotoHomeFragment() {
        baseActivity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
    }

    @Override
    public void onLocationReceived(Location location) {

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {

        if (controller.equalsIgnoreCase("journey") && action.equalsIgnoreCase("detail")) {
            if (status) {

                RideDetails journeyDetail = null;
                try {
                    journeyDetail = baseActivity.parseRideDetails(jsonObject.getJSONObject("list"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (journeyDetailListener != null) {
                    log("syncsuccess journeydetailListener: " + journeyDetailListener);
                    journeyDetailListener.updateDetail(journeyDetail);
                }
            }
        }
    }

    protected DataUtil getDataUtil(){
        if(dataUtil == null){
            try {
                dataUtil = new DataUtil(baseActivity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dataUtil;
    }

}
