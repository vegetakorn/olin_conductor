package com.olin.driver.fragment;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.olin.driver.R;
import com.olin.driver.activity.LoginSignupActivity;
import com.olin.driver.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class ForgotPasswordFragment extends BaseFragment {
    private View view;
    private EditText emailET;
    private Button resetBT;
    private ImageView forgotIV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ((LoginSignupActivity) baseActivity).toolbar_titleTV.setText(getString(R.string.forgot_password));
        ((LoginSignupActivity) baseActivity).toolbar_imageIV.setVisibility(View.GONE);
        emailET = (EditText) view.findViewById(R.id.emailET);
        forgotIV = (ImageView) view.findViewById(R.id.forgotIV);
        forgotIV.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        resetBT = (Button) view.findViewById(R.id.resetBT);
        resetBT.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.resetBT:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        String email = emailET.getText().toString().trim();
        if (email.isEmpty()) {
            showToast(getString(R.string.please_enter_ypur_email));
        } else if (!baseActivity.isValidMail(email)) {
            showToast(getString(R.string.please_enter_valid_email));
        } else {
            sendToServer(email);
        }
    }

    private void sendToServer(String email) {
        RequestParams params = new RequestParams();
        params.put("User[email]", email);
        baseActivity.syncManager.sendToServer(Const.API_USER_RECOVER + "?role_id=" + Const.ROLE_DRIVER, params, this);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("recover")) {
            if (status) {
                showToast(jsonObject.optString("success"));
                baseActivity.getSupportFragmentManager().popBackStack();
            } else {
                if (jsonObject.has("error")) {
                    showToast(jsonObject.optString("error"));
                } else {
                    showToast(getString(R.string.error));
                }
            }
        }
    }
}