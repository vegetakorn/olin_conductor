package com.olin.driver.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 3/6/17.
 */

public class RideDetails implements Parcelable {
    public int id;
    public String amount;
    public String location;
    public String destination;
    public double location_lat;
    public double location_long;
    public double destination_lat;
    public double destination_long;
    public String number_of_passengers;
    public String number_of_bags;
    public String number_of_hours;
    public boolean is_hourly;
    public boolean is_pet;
    public int journey_type;
    public int state_id;
    public String journey_time;
    public String start_time;
    public String end_time;
    public String create_time;
    public PassengerDetail passengerDetail;
    public CountryData countryData;
    public String image_file;
    public ProfileData profileData;


    public RideDetails(Parcel in) {
        id = in.readInt();
        amount = in.readString();
        location = in.readString();
        destination = in.readString();
        location_lat = in.readDouble();
        location_long = in.readDouble();
        destination_lat = in.readDouble();
        destination_long = in.readDouble();
        number_of_passengers = in.readString();
        number_of_bags = in.readString();
        number_of_hours = in.readString();
        is_hourly = in.readByte() != 0;
        is_pet = in.readByte() != 0;
        journey_type = in.readInt();
        state_id = in.readInt();
        journey_time = in.readString();
        start_time = in.readString();
        end_time = in.readString();
        create_time = in.readString();
        passengerDetail = in.readParcelable(PassengerDetail.class.getClassLoader());
        countryData = in.readParcelable(CountryData.class.getClassLoader());
        image_file = in.readString();
        profileData = in.readParcelable(ProfileData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(amount);
        dest.writeString(location);
        dest.writeString(destination);
        dest.writeDouble(location_lat);
        dest.writeDouble(location_long);
        dest.writeDouble(destination_lat);
        dest.writeDouble(destination_long);
        dest.writeString(number_of_passengers);
        dest.writeString(number_of_bags);
        dest.writeString(number_of_hours);
        dest.writeByte((byte) (is_hourly ? 1 : 0));
        dest.writeByte((byte) (is_pet ? 1 : 0));
        dest.writeInt(journey_type);
        dest.writeInt(state_id);
        dest.writeString(journey_time);
        dest.writeString(start_time);
        dest.writeString(end_time);
        dest.writeString(create_time);
        dest.writeParcelable(passengerDetail, flags);
        dest.writeParcelable(countryData, flags);
        dest.writeString(image_file);
        dest.writeParcelable(profileData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RideDetails> CREATOR = new Creator<RideDetails>() {
        @Override
        public RideDetails createFromParcel(Parcel in) {
            return new RideDetails(in);
        }

        @Override
        public RideDetails[] newArray(int size) {
            return new RideDetails[size];
        }
    };
}

