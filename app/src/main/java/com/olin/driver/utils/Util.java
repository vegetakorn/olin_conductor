package com.olin.driver.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import com.olin.driver.BuildConfig;

public class Util {
    /**
     * Metodo para hacer debug de la app
     * @param tag
     * @param message
     */
    public static  void log(String tag,String message){
        if (BuildConfig.DEBUG){
            Log.e(tag,message);
        }
    }

    public static boolean isDebug(){
        return BuildConfig.DEBUG;
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
