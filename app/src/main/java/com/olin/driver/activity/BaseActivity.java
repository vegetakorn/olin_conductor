package com.olin.driver.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.olin.driver.BuildConfig;
import com.olin.driver.R;
import com.olin.driver.data.CountryData;
import com.olin.driver.data.PassengerDetail;
import com.olin.driver.data.ProfileData;
import com.olin.driver.data.RideData;
import com.olin.driver.data.RideDetails;
import com.olin.driver.service.LocationUpdateService;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.GoogleApisHandle;
import com.olin.driver.utils.ImageUtils;
import com.olin.driver.utils.LatLngInterpolator;
import com.olin.driver.utils.NetworkUtil;
import com.olin.driver.utils.PrefStore;
import com.olin.driver.utils.TouchImageView;
import com.toxsl.volley.AppExpiredError;
import com.toxsl.volley.AppInMaintenance;
import com.toxsl.volley.AuthFailureError;
import com.toxsl.volley.NetworkError;
import com.toxsl.volley.ParseError;
import com.toxsl.volley.Request;
import com.toxsl.volley.ServerError;
import com.toxsl.volley.TimeoutError;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.RequestParams;
import com.toxsl.volley.toolbox.SyncEventListner;
import com.toxsl.volley.toolbox.SyncManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import me.tangke.slidemenu.SlideMenu;

/**
 * Created by tx-ubuntu-207 on 2/7/15.
 */
public class BaseActivity extends AppCompatActivity implements SyncEventListner, View.OnClickListener {

    private static String TAG = "BaseActivity";
    private static final int PERM_REQ_ACCESS_FINE_LOCATION = 9091;
    private static final int PERM_REQ_ACCESS_COARSE_LOCATION = 9092;
    public static GoogleApisHandle googleApiHandle;
    public LayoutInflater inflater;
    public SyncManager syncManager;
    public PrefStore store;
    public PermCallback permCallback;
    public Picasso picasso;
    public LocationUpdateService locationUpdateService;
    public SlideMenu slideMenu;
    public Toolbar toolbar;
    public TextView toolbar_titleTV;
    public ImageView toolbar_imageIV;
    public boolean is_profile_edit;
    public boolean is_vehicle_edit;
    private Toast toast;
    private Dialog progressDialog;
    private TextView txtMsgTV;
    private int reqCode;
    private NetworksBroadcast networksBroadcast;
    private AlertDialog networkAlertDialog;
    private String networkStatus;
    private InputMethodManager inputMethodManager;
    private android.app.AlertDialog.Builder failureDailog;
    private AlertDialog.Builder networkDialog;
    private boolean exit;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Const.DISPLAY_MESSAGE_ACTION)) {
                Bundle data = intent.getBundleExtra("map");
            } else if (intent.getAction().equalsIgnoreCase(Const.FCM_TOKEN_REFRESH)) {
                Bundle fcmBundle = intent.getExtras();
                updateTokenInServer(fcmBundle.getString(Const.FCM_TOKEN_REFRESH));
            }
        }
    };

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        syncManager = SyncManager.getInstance(this, BuildConfig.DEBUG);
        //syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name));
        (BaseActivity.this).overridePendingTransition(R.anim.slide_in,
                R.anim.slide_out);
        inputMethodManager = (InputMethodManager) this
                .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
        store = new PrefStore(this);
        locationUpdateService = LocationUpdateService.getInstance(this);
        googleApiHandle = GoogleApisHandle.getInstance(this);
        checkLanguage();
        registerFCM();
        initializeNetworkBroadcast();
        createPicassoDownloader();
        strictModeThread();
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        transitionSlideInHorizontal();
        progressDialog();
        failureDailog = new android.app.AlertDialog.Builder(this);
        networkDialog = new AlertDialog.Builder(this);

        //ExceptionHandler.register(this);
    }

    public void checkLanguage() {
        String language;
        String languageToLoad = "es";
        try {
            if (store.getString("language") == null)
                language = "Spanish";
            else
                language = store.getString("language");

            if (language.equalsIgnoreCase("Spanish")) {
                languageToLoad = "es";

            } else if (language.equalsIgnoreCase("English")) {
                languageToLoad = "en";
            }

            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name), languageToLoad);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createPicassoDownloader() {
        OkHttpClient okHttpClient = new OkHttpClient();
        picasso = new Picasso.Builder(this)
                .downloader(new OkHttpDownloader(okHttpClient))
                .build();
    }

    private void initializeNetworkBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        networksBroadcast = new NetworksBroadcast();
        registerReceiver(networksBroadcast, intentFilter);
    }

    private void registerFCM() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.DISPLAY_MESSAGE_ACTION);
        intentFilter.addAction(Const.FCM_TOKEN_REFRESH);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Intent intent = new Intent(Const.FCM_TOKEN_REFRESH);
                        intent.putExtra(Const.FCM_TOKEN_REFRESH, token);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }
                });
    }

    public void setActionBarTitle(String title, boolean showToolbar, int mipmap, boolean useImage) {
        toolbar_titleTV.setText(title);
        toolbar_titleTV.setVisibility(title.isEmpty() || title.equalsIgnoreCase("") ? View.GONE : View.VISIBLE);
        toolbar_imageIV.setBackgroundResource(useImage ? mipmap : 0);
        toolbar_imageIV.setVisibility(useImage ? View.VISIBLE : View.GONE);
        toolbar.setVisibility(showToolbar ? View.VISIBLE : View.GONE);
    }

    public void setActionBarTitle(String title, boolean showToolbar) {
        setActionBarTitle(title, showToolbar, 0, false);
    }

    private void updateTokenInServer(String token) {
        RequestParams requestParams = new RequestParams();
        Log.e("BaseActivity", "------device_token----- " + token);
        requestParams.put("AuthSession[device_token]", token);
        syncManager.sendToServer("user/check", requestParams, null);
    }

    private void unregisterNetworkBroadcast() {
        try {
            if (networksBroadcast != null) {
                unregisterReceiver(networksBroadcast);
            }
        } catch (IllegalArgumentException e) {
            networksBroadcast = null;
        }
    }

    public ProfileData parseProfileData(JSONObject detail) throws JSONException {
        Gson gson = new Gson();

        //ProfileData profileData = new ProfileData(Parcel.obtain());
        ProfileData profileData = gson.fromJson(detail.toString(),ProfileData.class);
        //profileData.image_file = detail.getString("image_file");
        //profileData.first_name = detail.getString("first_name");
        //profileData.last_name = detail.getString("last_name");
        //profileData.email = detail.getString("email");
        //profileData.contact_no = detail.getString("contact_no");
        //profileData.id = detail.getString("id");
        profileData.is_available = detail.getString("is_online");
        //profileData.driver_profile_step = detail.getInt("driver_profile_step");
        store.saveString(Const.IS_AVAILABLE, profileData.is_available);
        profileData.is_online = detail.getString("is_online");
        if (detail.has("driver_rating")) {
            profileData.rating = detail.getString("driver_rating");
        } else
            profileData.rating = BuildConfig.DEBUG ? "5" : "0";

        if (detail.has("country")) {
            JSONObject country = detail.getJSONObject("country");
            profileData.phone_code = country.getString("telephone_code");

            CountryData countryData = new CountryData(Parcel.obtain());
            countryData.id = country.optInt("id");
            countryData.title = country.optString("title");
            countryData.telephone_code = country.optString("telephone_code");
            countryData.flag = country.optString("flag");
            countryData.country_code = country.optString("country_code");
            countryData.currency_symbol = country.optString("currency_symbol");
            if (country.has("car_type")) {
                JSONArray car_type = country.optJSONArray("car_type");
                int[] carTypes = new int[car_type.length()];
                for (int j = 0; j < car_type.length(); j++) {
                    carTypes[j] = (int) car_type.opt(j);
                    countryData.arr_car_type = carTypes;
                }
            }
            profileData.countryData = countryData;
        }

        JSONObject drivrObjct = detail.getJSONObject("driver");
        profileData.vehicle = drivrObjct.optString("vehicle");
        profileData.model = drivrObjct.getString("model");
        profileData.license_no = drivrObjct.getString("license_no");
        profileData.registration_no = drivrObjct.getString("registration_no");
        if(!drivrObjct.isNull("type_id")){
            profileData.type_id = drivrObjct.getInt("type_id");
        }
        profileData.car_make = drivrObjct.getString("car_make");

        if (detail.has("imageProof")) {
            JSONObject imageProof = detail.getJSONObject("imageProof");
            profileData.id_proof_file = imageProof.getString("id_proof_file");
            profileData.license_file = imageProof.getString("license_file");
            profileData.document_file = imageProof.getString("document_file");
            profileData.vehicle_image_file = imageProof.getString("vehicle_image");
        }
        return profileData;
    }

    public RideData parseRideData(JSONObject detail) throws JSONException {
        RideData rideData = new RideData(Parcel.obtain());
        rideData.id = detail.getString("id");
        rideData.charge_mode = detail.getString("charge_mode");
        rideData.amount = detail.getString("amount");
        rideData.base_type = detail.getInt("base_type");
        rideData.pickupLat = detail.getDouble("location_lat");
        rideData.pickupLong = detail.getDouble("location_long");
        rideData.destinationLat = detail.getDouble("destination_lat");
        rideData.destinationLong = detail.getDouble("destination_long");
        rideData.destinationAddress = detail.getString("destination");
        rideData.journeyTime = detail.getString("journey_time");
        rideData.payment_mode = detail.getInt("payment_mode");
        rideData.number_of_passengers = detail.getInt("number_of_passengers");
        rideData.number_of_bags = detail.getInt("number_of_bags");
        rideData.state_id = detail.getInt("state_id");
        rideData.type_id = detail.getInt("type_id");

        return rideData;
    }

    protected void allParent() {

    }
    public void check() {
        String[] perms;
        perms = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            perms = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
            };
        }

        boolean perm1 = checkPermissions(perms, PERM_REQ_ACCESS_FINE_LOCATION, new PermCallback() {
            @Override
            public void permGranted(int resultCode) {
                checkToken();
            }

            @Override
            public void permDenied(int resultCode) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        check();
                    }
                }, 2000);
            }
        });
        /*
        boolean perm2 = checkPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERM_REQ_ACCESS_COARSE_LOCATION, new PermCallback() {
            @Override
            public void permGranted(int resultCode) {
                check();
            }

            @Override
            public void permDenied(int resultCode) {
                check();
            }
        });
        */
        if(perm1){
            checkToken();
        }
    }

    private void checkToken(){
        RequestParams params = new RequestParams();
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("AuthSession[device_token]", token);
        else
            params.put("AuthSession[device_token]", getUniqueDeviceId());
        syncManager.sendToServer("user/check", params, this);
    }
    private void showNoNetworkDialog(String status) {
        networkStatus = status;
        networkDialog.setTitle(getString(R.string.netwrk_status));
        networkDialog.setMessage(status);
        networkDialog.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isNetworkAvailable()) {
                    dialog.dismiss();
                    showNoNetworkDialog(networkStatus);
                }
            }
        });
        networkDialog.setCancelable(false);
        networkAlertDialog = networkDialog.create();
        if (!networkAlertDialog.isShowing())
            networkAlertDialog.show();
    }

    public String changeDateFormat(String dateString, String sourceDateFormat, String targetDateFormat) {
        if (dateString == null || dateString.isEmpty()) {
            return "";
        }
        SimpleDateFormat inputDateFromat = new SimpleDateFormat(sourceDateFormat, Locale.getDefault());
        Date date = new Date();
        try {
            date = inputDateFromat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(date);
    }

    public String changeDateFormatFromDate(Date sourceDate, String targetDateFormat) {
        if (sourceDate == null || targetDateFormat == null || targetDateFormat.isEmpty()) {
            return "";
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(sourceDate);
    }

    public String changeDateFormatFromUTCdate(String sourceDate, String currentFormat, String targetFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(currentFormat);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date myDate = null;
        try {
            myDate = simpleDateFormat.parse(sourceDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return changeDateFormatFromDate(myDate, targetFormat);
    }

    /**
     * Conviete una fecha UTC del serivdor a la hora local del usaurio
     * @param dateFomratOutPut
     * @param datesToConvert
     * @return
     */
    public static String convertUTCToLocal(String datesToConvert,String dateFomratOutPut) {
        String fotmatFromServer = "yyyy-MM-dd hh:mm:ss";
        String dateToReturn = datesToConvert;

        SimpleDateFormat sdf = new SimpleDateFormat(fotmatFromServer);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date gmt = null;
        //Util.log(TAG,"convertUTCToLocal timezone: "+TimeZone.getDefault().getDisplayName());
        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat(dateFomratOutPut);
        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());

        try {

            gmt = sdf.parse(datesToConvert);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }

    protected void checkDate(String checkDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date serverDate = null;
        try {
            serverDate = dateFormat.parse(checkDate);
            cal.setTime(serverDate);
            Calendar currentcal = Calendar.getInstance();
            if (currentcal.after(cal)) {
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setMessage("Please contact : shiv@toxsl.com");
                builder.setTitle("Demo Expired");
                builder.setCancelable(false);
                builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitFromApp();
                    }
                });
                androidx.appcompat.app.AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, Const.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                log(getString(R.string.this_device_is_not_supported));
                finish();
            }
            return false;
        }
        return true;
    }

    public boolean checkPermissions(String[] perms, int requestCode, PermCallback permCallback) {
        this.permCallback = permCallback;
        this.reqCode = requestCode;
        ArrayList<String> permsArray = new ArrayList<>();
        boolean hasPerms = true;
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                permsArray.add(perm);
                hasPerms = false;
            }
        }
        if (!hasPerms) {
            String[] permsString = new String[permsArray.size()];
            for (int i = 0; i < permsArray.size(); i++) {
                permsString[i] = permsArray.get(i);
            }
            ActivityCompat.requestPermissions(BaseActivity.this, permsString, 99);
            return false;
        } else
            return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permGrantedBool = false;
        switch (requestCode) {
            case 99:
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showToast(getString(R.string.not_sufficient_permissions)
                                + getString(R.string.app_name)
                                + getString(R.string.permissionss));
                        permGrantedBool = false;
                        break;
                    } else {
                        permGrantedBool = true;
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback.permGranted(reqCode);
                    } else {
                        permCallback.permDenied(reqCode);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageUtils.activityResult(requestCode, resultCode, data);
    }

    public void exitFromApp() {
        finish();
        finishAffinity();
    }

    public String getUniqueDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public boolean hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnectedOrConnecting();

    }

    public boolean isValidMail(String email) {
        return email.matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
    }

    public boolean isValidPassword(String password) {
        return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[!&^%$#@()=*/.+_-])(?=\\S+$).{8,}$");
    }

    public void keyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:>>>>>>>>>>>>>>>", "" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void log(String string) {
        if (BuildConfig.DEBUG)
            Log.e("BaseActivity", string);
    }

    private void progressDialog() {
        progressDialog = new Dialog(this);
        View view = View.inflate(this, R.layout.progress_dialog, null);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(view);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txtMsgTV = (TextView) view.findViewById(R.id.txtMsgTV);
        progressDialog.setCancelable(false);
    }

    public void startProgressDialog() {
        if (progressDialog != null && !progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void saveProfileDataInPrefStore(ProfileData profileData) {
        Gson gson = new Gson();
        String json = gson.toJson(profileData);
        store.saveString("profile_data", json);
    }

    public ProfileData getProfileDataFromPrefStore() {
        Gson gson = new Gson();
        ProfileData profileData = gson.fromJson(store.getString("profile_data"), ProfileData.class);
        return profileData;
    }

    @Override
    public void onSyncStart() {
        startProgressDialog();
    }

    @Override
    public void onSyncFinish() {
        stopProgressDialog();
    }

    android.app.AlertDialog.Builder errorDialog(String errorString, final Request mRequest) {
        failureDailog.setMessage(errorString).setCancelable(false).setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setPositiveButton(getString(R.string.retryy), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onSyncStart();
                syncManager.getRequestQueue().add(mRequest);
            }
        });
        return failureDailog;
    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {
        handleSyncError(error, mRequest);
    }

    private void handleSyncError(VolleyError error, Request mRequest) {
        if (error instanceof NetworkError) {
            if (!isFinishing())
                errorDialog(getString(R.string.request_timeout_slow_connection), mRequest).show();
        } else if (error instanceof ServerError) {
            showToast(getString(R.string.problem_connecting_to_the_server));
        } else if (error instanceof AuthFailureError) {
            showToast(getString(R.string.session_timeout_redirecting));
            syncManager.setLoginStatus(null);
            gotoLogin();
            //--------------------------------------------------------------------------------------goToLogin------------------------------------------------
        } else if (error instanceof ParseError) {
            showToast(getString(R.string.bad_request));
        } else if (error instanceof TimeoutError) {
            errorDialog(getString(R.string.request_timeout_slow_connection), mRequest).show();
        } else if (error instanceof AppExpiredError)
            checkDate(error.getMessage());
        else if (error instanceof AppInMaintenance)
            errorDialog(error.getMessage(), null).show();
        else if (!error.apiCrash.equals(""))
            showToast(error.apiCrash);
    }

    public void gotoLogin() {
        startActivity(new Intent(this, LoginSignupActivity.class));
        finish();
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                JSONObject detail = jsonObject.optJSONObject("detail");
                parseRideDetails(detail);
            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("user") && action.equals("check")) {
            if (status) {
                JSONObject object = null;
                try {
                    object = jsonObject.getJSONObject("detail");
                    ProfileData driverData = parseProfileData(object);
                    Intent intent = new Intent(this, MainActivity.class);
                    assert object != null;
                    if (object.has("last_ride")) {
                        RideDetails rideDetails = parseRideDetails(object.getJSONObject("last_ride"));
                        if (rideDetails != null) {
                            intent.putExtra("rideDetail", rideDetails);
                        }

                    }
                    saveProfileDataInPrefStore(driverData);
                    if (driverData.driver_profile_step == 2) {
                        startActivity(intent);
                    } else {
                        Intent signup = new Intent(this, LoginSignupActivity.class);
                        intent.putExtra("driver_profile_step", driverData.driver_profile_step);
                        startActivity(signup);
                    }
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                gotoLogin();
            }
        }
    }

    public RideDetails parseRideDetails(JSONObject detail) {
        if (detail != null) {
            RideDetails rideDetails = new RideDetails(Parcel.obtain());
            rideDetails.id = detail.optInt("id");
            store.setInt(Const.JOURNEY_ID, rideDetails.id);
            rideDetails.amount = detail.optString("amount");
            rideDetails.location = detail.optString("location");
            rideDetails.destination = detail.optString("destination");
            rideDetails.location_lat = detail.optDouble("location_lat");
            rideDetails.location_long = detail.optDouble("location_long");
            rideDetails.destination_lat = detail.optDouble("destination_lat");
            rideDetails.destination_long = detail.optDouble("destination_long");
            rideDetails.number_of_passengers = detail.optString("number_of_passengers");
            rideDetails.number_of_bags = detail.optString("number_of_bags");
            rideDetails.is_hourly = detail.optBoolean("is_hourly");
            rideDetails.is_pet = detail.optBoolean("is_pet");
            if (rideDetails.is_hourly) {
                rideDetails.number_of_hours = detail.optString("number_of_hours");
            }
            rideDetails.journey_time = detail.optString("journey_time");
            rideDetails.journey_type = detail.optInt("journey_type");
            rideDetails.start_time = detail.optString("start_time");
            rideDetails.end_time = detail.optString("end_time");
            rideDetails.create_time = detail.optString("create_time");
            rideDetails.state_id = detail.optInt("state_id");

            if (detail.has("passenger_detail")) {
                JSONObject passenger_detail = detail.optJSONObject("passenger_detail");
                PassengerDetail passengerDetail = new PassengerDetail(Parcel.obtain());
                passengerDetail.id = passenger_detail.optInt("id");
                passengerDetail.first_name = passenger_detail.optString("first_name");
                passengerDetail.last_name = passenger_detail.optString("last_name");
                passengerDetail.email = passenger_detail.optString("email");
                passengerDetail.contact_no = passenger_detail.optString("contact_no");
                passengerDetail.state_id = passenger_detail.optString("state_id");
                passengerDetail.passenger_rating = passenger_detail.optInt("passenger_rating");
                passengerDetail.image_file = passenger_detail.optString("image_file");

                if (passenger_detail.has("country")) {
                    CountryData countryData = new CountryData(Parcel.obtain());
                    JSONObject country = passenger_detail.optJSONObject("country");
                    countryData.id = country.optInt("id");
                    countryData.title = country.optString("title");
                    countryData.telephone_code = country.optString("telephone_code");
                    countryData.flag = country.optString("flag");
                    countryData.country_code = country.optString("country_code");
                    countryData.currency_symbol = country.optString("currency_symbol");
                    if (country.has("car_type")) {
                        JSONArray car_type = country.optJSONArray("car_type");
                        int[] carTypes = new int[car_type.length()];
                        for (int j = 0; j < car_type.length(); j++) {
                            carTypes[j] = (int) car_type.opt(j);
                            countryData.arr_car_type = carTypes;
                        }
                    }
                    passengerDetail.countryData = countryData;
                }
                rideDetails.passengerDetail = passengerDetail;
            }
            store.setInt(Const.STATE_ID, rideDetails.state_id);
            return rideDetails;
        }
        return null;
    }

    public void showToast(String msg) {
        toast.setText(msg);
        toast.show();
    }

    public void cancelNotification(int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
        if (notifyId == Const.ALL_NOTI)
            nMgr.cancelAll();
        else
            nMgr.cancel(notifyId);
    }

    private void strictModeThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void transitionSlideInHorizontal() {
        this.overridePendingTransition(R.anim.slide_in_right,
                R.anim.slide_out_left);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkBroadcast();
    }

    public void showFullScreenImage(String url) {
        try {
            picasso.load(url).placeholder(R.mipmap.default_avatar).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    displayImage(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showFullScreenImage(Bitmap bitmap) {
        displayImage(bitmap);
    }

    private void displayImage(Bitmap bitmap) {
        View view = View.inflate(this, R.layout.view_full_screen_image, null);
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setView(view);

        ImageView closeIV = (ImageView) view.findViewById(R.id.closeIV);
        TouchImageView imageTIV = (TouchImageView) view.findViewById(R.id.imageTIV);
        imageTIV.setImageBitmap(bitmap);
        androidx.appcompat.app.AlertDialog dialogg = builder.create();
        closeIV.setTag(dialogg);
        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                androidx.appcompat.app.AlertDialog alert = (androidx.appcompat.app.AlertDialog) v.getTag();
                alert.dismiss();
            }
        });
        dialogg.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (!dialogg.isShowing())
            dialogg.show();

        dialogg.getWindow().setLayout(getDisplaySize(), getDisplaySize());

    }

    public int getDisplaySize() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        return height < width ? height : width;
    }

    public void back() {
        if (exit) {
            finishAffinity();
        } else {
            Toast.makeText(this, "Press again in order to exit TaxiSitio", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    public void animateMarkerToGB(LatLng finalPosition, Marker oldMarker, boolean isCameraAnimate, GoogleMap googleMap) {
        LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();
        LatLng startPosition = oldMarker.getPosition();
        double lat1 = startPosition.latitude;
        double lng1 = startPosition.longitude;
        double lat2 = finalPosition.latitude;
        double lng2 = finalPosition.longitude;
        long start = SystemClock.uptimeMillis();
        Interpolator interpolator = new AccelerateDecelerateInterpolator();
        float durationInMs = 9 * 1000;
        Handler handler = new Handler();
        googleApiHandle.rotateMarker(lat1, lng1, lat2, lng2, oldMarker, handler);
        handler.post(new MyRunnable(start, durationInMs, interpolator, oldMarker, latLngInterpolator, startPosition, finalPosition, isCameraAnimate, googleMap, handler) {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {                 // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                oldMarker.setPosition(latLngInterpolator.interpolate(v, startPosition, finalPosition));
                if (isCameraAnimate) {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(startPosition);
                    builder.include(finalPosition);
                    LatLngBounds bounds = builder.build();
                    int padding = 10;
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                            padding);
                    googleMap.animateCamera(cu);
                }
                //Repeat till progress is complete.
                if (t < 1) {                     // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public interface PermCallback {
        void permGranted(int resultCode);

        void permDenied(int resultCode);
    }

    public class NetworksBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = NetworkUtil.getConnectivityStatusString(context);
            if (status == null && networkAlertDialog != null) {
                networkStatus = null;
                networkAlertDialog.dismiss();
                allParent();
            } else if (status != null && networkStatus == null)
                showNoNetworkDialog(status);
        }
    }

    class MyRunnable implements Runnable {
        Interpolator interpolator;
        Marker oldMarker;
        LatLngInterpolator latLngInterpolator;
        LatLng startPosition;
        LatLng finalPosition;
        boolean isCameraAnimate;
        GoogleMap googleMap;
        Handler handler;
        float durationInMs;
        long start;

        public MyRunnable(long start, float durationInMs, Interpolator interpolator, Marker oldMarker, LatLngInterpolator latLngInterpolator, LatLng startPosition, LatLng finalPosition, boolean isCameraAnimate, GoogleMap googleMap, Handler handler) {
            this.start = start;
            this.durationInMs = durationInMs;
            this.interpolator = interpolator;
            this.oldMarker = oldMarker;
            this.latLngInterpolator = latLngInterpolator;
            this.startPosition = startPosition;
            this.finalPosition = finalPosition;
            this.isCameraAnimate = isCameraAnimate;
            this.googleMap = googleMap;
            this.handler = handler;
        }

        @Override
        public void run() {

        }
    }


}