package com.olin.driver.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TOXSL\visha.sehgal on 27/1/17.
 */

public class ProfileData implements Parcelable {
    public String full_name;

    @SerializedName("first_name")
    public String first_name;

    @SerializedName("last_name")
    public String last_name;

    @SerializedName("contact_no")
    public String contact_no;

    @SerializedName("email")
    public String email;

    @SerializedName("image_file")
    public String image_file;
    public String password;
    public int country_id;
    public String phone_code;
    public String vehicle;
    public String model;
    public String license_no;
    public String registration_no;
    public int type_id;

    @SerializedName("driver_profile_step")
    public int driver_profile_step;
    public String id_proof_file;
    public String license_file;
    public String document_file;
    public String vehicle_image_file;

    @SerializedName("id")
    public String id;

    public String is_available;
    public String is_online;
    public String rating;
    public String car_make;
    public String state;
    public String postalcode;

    public CountryData countryData;
    public Vehicle vehicleData;

    @SerializedName("driver")
    public Driver driver;

    public ProfileData(Parcel in) {
        full_name = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        contact_no = in.readString();
        email = in.readString();
        image_file = in.readString();
        password = in.readString();
        country_id = in.readInt();
        phone_code = in.readString();
        vehicle = in.readString();
        model = in.readString();
        license_no = in.readString();
        registration_no = in.readString();
        type_id = in.readInt();
        driver_profile_step = in.readInt();
        id_proof_file = in.readString();
        license_file = in.readString();
        document_file = in.readString();
        vehicle_image_file = in.readString();
        id = in.readString();
        is_available = in.readString();
        is_online = in.readString();
        rating = in.readString();
        car_make = in.readString();
        postalcode = in.readString();
        state = in.readString();
        countryData = in.readParcelable(CountryData.class.getClassLoader());
        vehicleData = in.readParcelable(Vehicle.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full_name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(contact_no);
        dest.writeString(email);
        dest.writeString(image_file);
        dest.writeString(password);
        dest.writeInt(country_id);
        dest.writeString(phone_code);
        dest.writeString(vehicle);
        dest.writeString(model);
        dest.writeString(license_no);
        dest.writeString(registration_no);
        dest.writeInt(type_id);
        dest.writeInt(driver_profile_step);
        dest.writeString(id_proof_file);
        dest.writeString(license_file);
        dest.writeString(document_file);
        dest.writeString(vehicle_image_file);
        dest.writeString(id);
        dest.writeString(is_available);
        dest.writeString(is_online);
        dest.writeString(rating);
        dest.writeString(car_make);
        dest.writeString(postalcode);
        dest.writeString(state);
        dest.writeParcelable(countryData, flags);
        dest.writeParcelable(vehicleData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileData> CREATOR = new Creator<ProfileData>() {
        @Override
        public ProfileData createFromParcel(Parcel in) {
            return new ProfileData(in);
        }

        @Override
        public ProfileData[] newArray(int size) {
            return new ProfileData[size];
        }
    };
}
