package com.olin.driver.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.olin.driver.R;
import com.olin.driver.activity.BaseActivity;
import com.olin.driver.data.RideDetails;
import com.olin.driver.fragment.MyReservationsFragment;
import com.olin.driver.utils.Const;

import java.util.ArrayList;

/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class ReservationsAdapter extends ArrayAdapter<RideDetails> {
    private BaseActivity context;
    ArrayList<RideDetails> rideDetailList = new ArrayList<>();
    MyReservationsFragment fragment;
    String[] states = new String[]{"NA", "New", "Accepted", "Rejected", "Canceled", "Driver Arrived", "Started", "Completed", "Paid"};

    public ReservationsAdapter(BaseActivity context, ArrayList<RideDetails> rideDetailList, MyReservationsFragment fragment) {
        super(context, 0);
        this.context = context;
        this.rideDetailList = rideDetailList;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return rideDetailList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_booking_history, parent, false);
            holder.passengerNameTV = (TextView) convertView.findViewById(R.id.passengerNameTV);
            holder.pickUpLocTV = (TextView) convertView.findViewById(R.id.pickUpLocTV);
            holder.destinationTV = (TextView) convertView.findViewById(R.id.destinationTV);
            holder.rideStatusTV = (TextView) convertView.findViewById(R.id.rideStatusTV);
            holder.dateTimeTV = (TextView) convertView.findViewById(R.id.dateTimeTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        RideDetails details = rideDetailList.get(position);
        if (details.passengerDetail != null && !details.passengerDetail.first_name.isEmpty()) {
            holder.passengerNameTV.setText(details.passengerDetail.first_name.concat(" " + details.passengerDetail.last_name));
            if (details.state_id == Const.STATE_CANCELLED)
                holder.dateTimeTV.setText(!details.create_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.create_time, "dd MMM,yyyy hh:mm a") : "NA");
            else
                holder.dateTimeTV.setText(!details.start_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.start_time, "dd MMM,yyyy hh:mm a") : "NA");
        } else {
            holder.passengerNameTV.setText(context.getString(R.string.na));
            holder.dateTimeTV.setText(!details.create_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.create_time, "dd MMM,yyyy hh:mm a") : "NA");
        }
        holder.pickUpLocTV.setText(details.location);
        holder.destinationTV.setText(details.destination);

        holder.rideStatusTV.setText(states[details.state_id]);
        if (position + 1 == rideDetailList.size()) {
            fragment.hitRideHistoryApi();
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView rideStatusTV, passengerNameTV, pickUpLocTV, destinationTV, dateTimeTV;
    }

}