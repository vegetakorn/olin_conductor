package com.olin.driver.utils;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.olin.driver.R;
import com.olin.driver.activity.BaseActivity;
import com.olin.driver.data.Company;
import com.olin.driver.data.CountryData;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.SyncEventListner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataUtil implements SyncEventListner {

    private BaseActivity baseActivity;

    private String[] companiesArr;
    private Company[] companies;
    private Company companySelected;
    private EditText etChoiceCompany;

    public DataUtil(BaseActivity baseActivity) throws Exception {
        this.baseActivity = baseActivity;
        if(baseActivity == null){
            throw new Exception("baseActivity no puede estar nulo.");
        }
    }

    public void initChoiceCompany(final EditText etCompany){
        etCompany.setFocusable(false);
        etCompany.setHint(R.string.company);

        etCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companiesShowOptionsDialog();
            }
        });
        this.etChoiceCompany = etCompany;
    }

    public void loadCompanies(CountryData country){
        baseActivity.syncManager.sendToServer(Const.API_COMPANY_INDEX+"?Company[enabled]=1&Company[country_id]="+country.id, null, DataUtil.this);
    }

    /**
     * Muestra las empresas
     */
    public void companiesShowOptionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity, R.style.myDialog);
        builder.setTitle(R.string.select_);
        builder.setItems(companiesArr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                etChoiceCompany.setText(companiesArr[i]);
                companySelected = companies[i];
            }
        });
        builder.setNegativeButton(baseActivity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                companySelected = null;
                etChoiceCompany.setText(null);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public Company getCompanySelected(){
        return companySelected;
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncFinish() {

    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        if (controller.equals("company") && action.equals("index") && status) {
            companySelected = null;
            try {
                JSONArray companiesJson = jsonObject.getJSONArray("list");
                if (companiesJson != null) {
                    companies = new Company[companiesJson.length()];
                    companiesArr = new String[companiesJson.length()];
                }
                for (int i = 0; i < companiesJson.length(); i++) {
                    JSONObject companyObj = companiesJson.getJSONObject(i);
                    Company companyData = new Company();
                    companyData.id = companyObj.getInt("id");
                    companyData.name = companyObj.getString("name");
                    companies[i] = companyData;
                    companiesArr[i] = companyData.name;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
