package com.olin.driver.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.olin.driver.R;
import com.olin.driver.activity.LoginSignupActivity;
import com.olin.driver.activity.MainActivity;
import com.olin.driver.data.ProfileData;
import com.olin.driver.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */

public class LoginSignupFragment extends BaseFragment {
    private Button fbLoginButton;
    private View view;
    private TextView emailLoginTV, passwordET, emailET;
    private TextView signupTV;
    private TextView forgotPasswordTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login_signup, container, false);
        ((LoginSignupActivity) baseActivity).toolbar.setVisibility(View.GONE);
        initUI();
        return view;
    }

    private void initUI() {
        emailLoginTV = (TextView) view.findViewById(R.id.emailLoginTV);
        emailET = (TextView) view.findViewById(R.id.emailET);
        passwordET = (TextView) view.findViewById(R.id.passwordET);
        fbLoginButton = (Button) view.findViewById(R.id.fbLoginButton);
        signupTV = (TextView) view.findViewById(R.id.signupTV);
        forgotPasswordTV = (TextView) view.findViewById(R.id.forgotPasswordTV);
        signupTV.setOnClickListener(this);
        forgotPasswordTV.setOnClickListener(this);
        emailLoginTV.setOnClickListener(this);
        fbLoginButton.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.emailLoginTV:
                if (validation()) {
                    Login();
                }

                break;
            case R.id.signupTV:
                goToSignUpScreen();
                break;
            case R.id.fbLoginButton:
                break;
            case R.id.forgotPasswordTV:
                goToForgotPasswordScreen();
                break;
        }
    }

    private void goToForgotPasswordScreen() {
        baseActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_container, new ForgotPasswordFragment())
                .addToBackStack(null)
                .commit();
    }

    private void goToSignUpScreen() {
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new SignUpFragment()).addToBackStack(null).commit();
    }

    private void Login() {
        baseActivity.store.saveString("user_email", "");
        baseActivity.store.saveString("user_password", "");
        baseActivity.store.setBoolean("isRememberMe", false);

        RequestParams params = new RequestParams();
        params.put("LoginForm[username]", emailET.getText().toString().trim());
        params.put("LoginForm[password]", passwordET.getText().toString().trim());
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("LoginForm[device_token]", token);
        else
            params.put("LoginForm[device_token]", baseActivity.getUniqueDeviceId());
        params.put("LoginForm[device_type]", 1);
        params.put("User[role_id]", 2);
        baseActivity.syncManager.sendToServer(Const.API_LOGIN, params, this);
    }

    private boolean validation() {
        if (emailET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_email));
        else if (passwordET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_password));
        else return true;
        return false;
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (action.equals("login") && controller.equals("user")) {
            if (status) {
                try {
                    JSONObject object = jsonObject.getJSONObject("user_detail");
                    ProfileData profileData = baseActivity.parseProfileData(object);
                    if (profileData.driver_profile_step == 2) {
                        baseActivity.saveProfileDataInPrefStore(profileData);
                        baseActivity.syncManager.setLoginStatus(jsonObject.getString("auth_code"));
                        baseActivity.startActivity(new Intent(baseActivity, MainActivity.class));
                        baseActivity.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    if (jsonObject.has("driver_profile_step")) {
                        if (jsonObject.getInt("driver_profile_step") == 1) {
                            showToast(getString(R.string.please_complete_doc));
                            gotoStepSecondSignup(jsonObject.getInt("id"));
                        } else {
                            showToast(jsonObject.getString("error"));
                        }
                    } else {
                        showToast(jsonObject.getString("error"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void gotoStepSecondSignup(int id) {
        Bundle bundle = new Bundle();
        SignupLicenseDetailFragment signupLicenseDetailFragment = new SignupLicenseDetailFragment();
        bundle.putInt("id", id);
        signupLicenseDetailFragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, signupLicenseDetailFragment).addToBackStack(null).commitAllowingStateLoss();
    }


}
