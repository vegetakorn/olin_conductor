package com.olin.driver.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.pkmmte.view.CircularImageView;
import com.olin.driver.R;
import com.olin.driver.activity.BaseActivity;
import com.olin.driver.activity.LoginSignupActivity;
import com.olin.driver.adapter.CustomSpinnerAdapter;
import com.olin.driver.data.CountryData;
import com.olin.driver.data.ProfileData;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 * Pagina de registro
 */

public class SignUpFragment extends BaseFragment implements BaseActivity.PermCallback, ImageUtils.ImageSelectCallback {
    private static final int VEHICLE = 1;
    private static final int PHONE = 2;
    private View view;
    private EditText firstNameET, lastNameET, emailET, passwordET, confirmPasswordET, phoneNumberET, /*dobET, */
            carModelET, carMakeET, typeOfCarET;
    private Button continueBT;
    private CircularImageView profileCIV;
    private ImageView selectImageIV;
    SimpleDateFormat simpleDateFormat;
    private File file;
    private ArrayList<CountryData> countryDatas = new ArrayList<>();
    private CustomSpinnerAdapter spinnerAdapter;
    private int country_id = 11;
    private DatePickerDialog datePicker;
    private String date;
    Calendar mycalender = Calendar.getInstance();
    private java.lang.String current_date;
    private SimpleDateFormat formater;
    private int[] car_typeID = new int[]{0, 1, 2};
    private int car_type_id = 10;
    private String[] car_type;
    private String[] arrCountry;
    private String id;
    private Spinner countrySP;

    private EditText etCompany;

    public String num = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.activity_signup, container, false);
            ((LoginSignupActivity) baseActivity).toolbar.setVisibility(View.VISIBLE);
            ((LoginSignupActivity) baseActivity).setToolbarTitle(baseActivity.getString(R.string.sign_up));
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            formater = new SimpleDateFormat("MMMM", Locale.US);
            current_date = simpleDateFormat.format(mycalender.getTime());
            initUI();
            createDatePicker();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCountryCode();
    }

    private void getCountryCode() {
        baseActivity.syncManager.sendToServer("user/country?id=", null, this);
    }

    private void initUI() {
        firstNameET = (EditText) view.findViewById(R.id.firstNameET);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        emailET = (EditText) view.findViewById(R.id.emailET);
        passwordET = (EditText) view.findViewById(R.id.passwordET);
        confirmPasswordET = (EditText) view.findViewById(R.id.confirmPasswordET);
        phoneNumberET = (EditText) view.findViewById(R.id.phoneNumberET);
        typeOfCarET = (EditText) view.findViewById(R.id.typeOfCarET);
        carMakeET = (EditText) view.findViewById(R.id.carMakeET);
        carModelET = (EditText) view.findViewById(R.id.carModelET);
        continueBT = (Button) view.findViewById(R.id.continueBT);
        countrySP = (Spinner) view.findViewById(R.id.countrySP);
        etCompany = (EditText) view.findViewById(R.id.etCompany);
        continueBT.setOnClickListener(this);

        typeOfCarET.setOnClickListener(this);
        /*
        if(Util.isDebug()){
            long t = System.currentTimeMillis();
            firstNameET.setText("Carlos");
            lastNameET.setText("Mendoza");
            emailET.setText("inhack20_"+t+"@mpandco.com");
            passwordET.setText("abc."+t);
            confirmPasswordET.setText("abc."+t);
            phoneNumberET.setText(""+t);
            carMakeET.setText("Ford");
            carModelET.setText("Ecosport");
        }
        */

        getDataUtil().initChoiceCompany(etCompany);

        countrySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryData dataItem = (CountryData) parent.getItemAtPosition(position);
                if (!(((CountryData) countrySP.getSelectedItem()).id == 0)) {
                    country_id = dataItem.id;
                    if (dataItem.arr_car_type != null && dataItem.arr_car_type.length != 0) {
                        car_type = null;
                        car_type = new String[dataItem.arr_car_type.length];
                        for (int i = 0; i < dataItem.arr_car_type.length; i++) {
                            switch (dataItem.arr_car_type[i]) {
                                case 0:
                                    if (car_type[0] == null)
                                    car_type[0] = Const.TYPE_CAR;
                                    else
                                        car_type[1] = Const.TYPE_CAR;
                                    break;
                                case 1:
                                    if (car_type[0] == null)
                                        car_type[0] = Const.TYPE_SUV;
                                    else
                                        car_type[1] = Const.TYPE_SUV;
                                    break;
                                case 2:
                                    if (car_type[0] == null && car_type[1] == null)
                                        car_type[0] = Const.TYPE_MINIVAN;
                                    else if (car_type[0] != null && car_type[1] == null)
                                        car_type[1] = Const.TYPE_MINIVAN;
                                    else
                                        car_type[2] = Const.TYPE_MINIVAN;
                                    break;
                            }
                        }
                    }
                    getDataUtil().loadCompanies(dataItem);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.continueBT:
                //buscamos si el nombre tiene un numero en la cadena
                String str = firstNameET.getText().toString();
                String s = "helloThisIsA1234Sample";
                s = str.replaceAll("[0-9]","");
                firstNameET.setText(s);
                //showToast(s);
               if (validation())
                    hitStepFirstApi();
                break;
            case R.id.selectImageIV:
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 123, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 123);
                    builder.start();
                }
                break;
            case R.id.typeOfCarET:
                showOptionsDialog();
                break;

        }
    }

    private void hitStepFirstApi() {
        RequestParams params = new RequestParams();
        params.put("User[first_name]", firstNameET.getText().toString().trim());
        params.put("User[last_name]", lastNameET.getText().toString().trim());
        params.put("User[email]", emailET.getText().toString().trim());
        params.put("User[password]", passwordET.getText().toString().trim());
        params.put("User[contact_no]", phoneNumberET.getText().toString().trim());
        params.put("User[country_id]", country_id);
        params.put("Driver[car_make]", carMakeET.getText().toString());
        params.put("Driver[vehicle]", car_type_id);
        params.put("Driver[model]", carModelET.getText().toString());
        params.put("Driver[company_id]", getDataUtil().getCompanySelected().id);
        baseActivity.syncManager.sendToServer(Const.API_STEP_1, params, this);
       // goToSignUpCarDetailsFragment();
    }

    private void createDatePicker() {
        datePicker = new DatePickerDialog(baseActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                date = simpleDateFormat.format(calendar.getTime());
                Date current, selected;
                try {
                    current = simpleDateFormat.parse(current_date);
                    selected = simpleDateFormat.parse(date);
                    if (current.equals(selected)) {

                    } else if (selected.before(current)) {


                    } else {
                        showToast(getResources().getString(R.string.enter_valid_date));
                        date = "";
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, mycalender.get(Calendar.YEAR), mycalender.get(Calendar.MONTH), mycalender.get(Calendar.DAY_OF_MONTH));
    }

    private void goToSignUpCarDetailsFragment() {
        Bundle bundle = new Bundle();
        SignupLicenseDetailFragment signupLicenseDetailFragment = new SignupLicenseDetailFragment();
        ProfileData signupData = new ProfileData(Parcel.obtain());
        signupData.first_name = firstNameET.getText().toString().trim();
        signupData.last_name = lastNameET.getText().toString().trim();
        signupData.email = emailET.getText().toString().trim();
        signupData.password = passwordET.getText().toString().trim();
        signupData.contact_no = phoneNumberET.getText().toString().trim();
        signupData.country_id = country_id;
        signupData.id = id;
        signupData.model = carModelET.getText().toString();
        signupData.vehicle = typeOfCarET.getText().toString();
        signupData.car_make = carMakeET.getText().toString();
        bundle.putParcelable("signupdata", signupData);
        signupLicenseDetailFragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, signupLicenseDetailFragment).addToBackStack(null).commitAllowingStateLoss();
    }


    private boolean validation() {

        if (firstNameET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_first_name));
        else if (lastNameET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_last_name));
        else if (emailET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_email));
        else if (!baseActivity.isValidMail(emailET.getText().toString().trim()))
            showToast(getString(R.string.please_enter_valid_email));
        else if (passwordET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_password));
        else if (passwordET.getText().toString().trim().length() < 8)
            showToast(getString(R.string.Password_should_be_minimum_8_characters));
        else if (!baseActivity.isValidPassword(passwordET.getText().toString().trim()))
            showToast(getString(R.string.password_should_contain_atleast_1_special_character));
        else if (confirmPasswordET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_confirm_password));
        else if (!(confirmPasswordET.getText().toString().trim().equals(passwordET.getText().toString().trim())))
            showToast(getString(R.string.password_doesnt_match));
        else if (country_id == 11)
            showToast(getString(R.string.please_select_country));
        else if (phoneNumberET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_phone_number));
        else if (typeOfCarET.getText().toString().isEmpty())
            showToast(getString(R.string.please_select_car_type));
        else if (carMakeET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_car_make));
        else if (carModelET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_car_model));
        else if (getDataUtil().getCompanySelected() == null)
            showToast(getString(R.string.please_select_company));
        else return true;
        return false;
    }


    @Override
    public void permGranted(int resultCode) {
        if (resultCode == 123) {
            ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 123);
            builder.start();
        }
    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
        if (resultCode == 123) {
            Bitmap bitmap = ImageUtils.imageCompress(imagePath);
            file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            profileCIV.setImageBitmap(bitmap);
        }

    }


    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("country")) {
            if (status) {
                try {
                    JSONArray country = jsonObject.getJSONArray("country");
                    if (country != null && country.length() != 0) {
                        arrCountry = new String[country.length()];
                    }
                    for (int i = 0; i < country.length(); i++) {
                        JSONObject countryObjct = country.getJSONObject(i);
                        CountryData countryData = new CountryData(Parcel.obtain());
                        countryData.id = countryObjct.getInt("id");
                        countryData.title = countryObjct.getString("title");
                        countryData.telephone_code = countryObjct.getString("telephone_code");
                        countryData.flag = countryObjct.getString("flag");
                        if (countryObjct.has("car_type")) {
                            if (countryObjct.getJSONArray("car_type").length() != 0) {
                                JSONArray car_type = countryObjct.optJSONArray("car_type");
                                int[] carTypes = new int[car_type.length()];
                                for (int j = 0; j < car_type.length(); j++) {
                                    carTypes[j] = (int) car_type.opt(j);
                                    countryData.arr_car_type = carTypes;
                                }
                                arrCountry[i] = countryData.title;
                                countryDatas.add(countryData);
                            }
                        }

                    }
                    spinnerAdapter = new CustomSpinnerAdapter(baseActivity, countryDatas);
                    countrySP.setAdapter(spinnerAdapter);
                    if (country_id == 11) {
                        for (int i = 0; i < countryDatas.size(); i++) {
                            if (countryDatas.get(i).telephone_code.equals("+1")) {
                                countrySP.setSelection(i);
                                break;
                            }

                        }
                    } else {
                        for (int i = 0; i < countryDatas.size(); i++) {
                            if (countryDatas.get(i).id == country_id) {
                                countrySP.setSelection(i);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if (controller.equals("user") && action.equals("driver-step1")) {
            if (status) {
                id = jsonObject.optJSONObject("detail").optString("id");
                goToSignUpCarDetailsFragment();


            } else {
                showToast(jsonObject.optString("error"));
            }
        }
    }

    /**
     * Muestra las opciones del tipo de vehiculo
     */
    public void showOptionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity, R.style.myDialog);
        builder.setTitle(R.string.select_);
        builder.setItems(car_type, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                typeOfCarET.setText(car_type[i]);
                switch (typeOfCarET.getText().toString()){
                    case Const.TYPE_CAR:
                        car_type_id =0;
                        break;
                    case Const.TYPE_SUV:
                        car_type_id =1;
                        break;
                    case Const.TYPE_MINIVAN:
                        car_type_id =2;
                        break;
                }

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}

