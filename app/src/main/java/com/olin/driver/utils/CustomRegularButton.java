package com.olin.driver.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by TOXSL\chirag.tyagi on 16/3/17.
 */

public class CustomRegularButton extends Button {
    public CustomRegularButton(Context context) {
        super(context);
        setFont();

    }

    public CustomRegularButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomRegularButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();

    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_Narrow_Book.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}
