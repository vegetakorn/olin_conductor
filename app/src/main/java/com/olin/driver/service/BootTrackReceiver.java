package com.olin.driver.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.olin.driver.utils.Util;

/**
 * Created by TOXSL\ankush.walia on 19/12/16.
 */

public class BootTrackReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("---BootTrackReceiver---", "-------come---------");
        if (!Util.isMyServiceRunning(LocationUpdateService.class, context))
            LocationUpdateService.startService(context);
    }
}
