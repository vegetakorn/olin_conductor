package com.olin.driver.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pkmmte.view.CircularImageView;
import com.olin.driver.R;
import com.olin.driver.activity.BaseActivity;
import com.olin.driver.activity.MainActivity;
import com.olin.driver.data.RideDetails;
import com.olin.driver.service.LocationUpdateService;
import com.olin.driver.utils.Const;
import com.olin.driver.utils.GoogleApisHandle;
import com.olin.driver.utils.Util;

import org.json.JSONObject;

import static com.olin.driver.activity.BaseActivity.googleApiHandle;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 * Vista que sale luego que el conductor recoje al pasajero
 */

public class StartRideFragment extends BaseFragment implements BaseActivity.PermCallback, GoogleApisHandle.OnPolyLineReceived, LocationUpdateService.OnLocationReceived {
    private static final int SETUP_MAP_LOCATION_PERM_REQ = 5;
    private static final String TAG = "StartRideFragment";
    private View view;
    private TextView passengerNameTV,pickUpLocationTV,tvDestination;
    private FloatingActionButton callTV,messagesTV,arrivalConfirmTV,startRideTV;
    private Button endRideTV;
    private CircularImageView passengerProfileCIV;
    private GoogleMap googlemap;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private int rideId;
    private RideDetails rideDetails;
    private int stateId;
    private Location currentLocation;
    private LinearLayout llInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_start_ride, container, false);
            ((MainActivity) getActivity()).setToolbar(getString(R.string.app_name));

            if (getArguments() != null && getArguments().containsKey("ride_id")) {
                rideId = getArguments().getInt("ride_id");

            }
            initUi();
        }
        Util.log(TAG,"onCreateView");
        return view;
    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    private void initUi() {
        llInfo = (LinearLayout) view.findViewById(R.id.llInfo);
        callTV = (FloatingActionButton) view.findViewById(R.id.callTV);
        messagesTV = (FloatingActionButton) view.findViewById(R.id.messagesTV);
        arrivalConfirmTV = (FloatingActionButton) view.findViewById(R.id.arrivalConfirmTV);
        pickUpLocationTV = (TextView) view.findViewById(R.id.pickUpLocTV);
        tvDestination = (TextView) view.findViewById(R.id.tvDestination);
        passengerProfileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        passengerNameTV = (TextView) view.findViewById(R.id.passengerNameTV);
        startRideTV = (FloatingActionButton) view.findViewById(R.id.startRideTV);
        endRideTV = (Button) view.findViewById(R.id.endRideTV);
        startRideTV.setOnClickListener(this);
        endRideTV.setOnClickListener(this);
        arrivalConfirmTV.setOnClickListener(this);
        messagesTV.setOnClickListener(this);
        callTV.setOnClickListener(this);
        arrivalConfirmTV.show();
        startRideTV.hide();
        endRideTV.setVisibility(View.GONE);
        initilizeMap();
    }


    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            map.getMapAsync(new OnMapReadyCallback() {
                @SuppressLint("MissingPermission")
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    googleMap.getUiSettings().setCompassEnabled(false);
                    googleMap.getUiSettings().setTiltGesturesEnabled(false);
                    googleMap.getUiSettings().setRotateGesturesEnabled(false);

                    googleMap.setMyLocationEnabled(true);//Se debe habilitar para que se actualice en tiempo real la ubicacion

                    StartRideFragment.this.googlemap = googleMap;
                    googleApiHandle.setPolyLineReceivedListener(StartRideFragment.this);
                }
            });
        }
        if (rideDetails != null && rideDetails.state_id == Const.STATE_ACCEPTED) {
            drawPolyLine();
        } else {
            UpdatePolyline();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCurrentLocation();
        if (locationUpdateService != null)
            locationUpdateService.setLocationReceivedListner(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rideDetailsBT:
                break;
            case R.id.arrivalConfirmTV:

                break;
            case R.id.startRideTV:
                startRide();
                break;
            case R.id.endRideTV:
                completeRide();
                break;
            case R.id.callTV:
                call(rideDetails.passengerDetail.countryData.telephone_code.concat(rideDetails.passengerDetail.contact_no));
                break;
            case R.id.messagesTV:
                message(rideDetails.passengerDetail.countryData.telephone_code.concat(rideDetails.passengerDetail.contact_no));
                break;
        }
    }

    private void completeRide() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_COMPLETED + rideId, null, this);
        /*
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.attention))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.to_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.attention_finish_complete_ride));
        builder.create().show();
        */
    }

    private void arrivalBuzz() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_ARRIVED + rideId, null, this);
    }

    private void startRide() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_STARTED + rideId, null, this);
    }


    private void setDriverMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(setCarType(Integer.parseInt(baseActivity.getProfileDataFromPrefStore().vehicle)))));
        else {
            pickupMarker.setPosition(pickupLatLng);
            pickupMarker.setIcon(BitmapDescriptorFactory.fromResource(setCarType(Integer.parseInt(baseActivity.getProfileDataFromPrefStore().vehicle))));
        }
    }


    private int setCarType(int type) {
        switch (type) {
            case 0:
                return R.mipmap.ic_car;
            case 1:
                return R.mipmap.ic_suv;
            case 2:
                return R.mipmap.ic_minivan;
        }
        return R.mipmap.ic_car;
    }

    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_green)));
        else {
            pickupMarker.setPosition(pickupLatLng);

        }
    }

    private void setDestinationMarker(LatLng dropoffLatLng, String time, String line) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_red));
            if (time != null && !time.isEmpty()) {
                options.title(baseActivity.getString(R.string.estimated_time));
                options.snippet("  " + time + " " + line);
            }
            destinationMarker = googlemap.addMarker(options);

        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle("");
                destinationMarker.setTitle(baseActivity.getString(R.string.estimated_time));
                destinationMarker.setSnippet("  " + time + " " + line);
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();

        }
    }


    private void drawPolyLine() {
        if (googlemap != null && rideDetails != null && currentLocation != null) {
            googleApiHandle.getDirectionsUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new LatLng(rideDetails.location_lat, rideDetails.location_long), googlemap);
        }
    }

    private void UpdatePolyline() {
        if (googlemap != null && rideDetails != null && currentLocation != null) {
            googleApiHandle.getDirectionsUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new LatLng(rideDetails.destination_lat, rideDetails.destination_long), googlemap);
        }
    }

    @Override
    public void onSyncStart() {
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                stateId = rideDetails.state_id;
                switch (stateId) {
                    case Const.STATE_CANCELLED:
                        gotoHomeFragment();
                        break;
                    case Const.STATE_DRIVER_ARRIVED:
                        startRideTV.show();
                        arrivalConfirmTV.hide();
                        break;
                    case Const.STATE_STARTED:
                        endRideTV.setVisibility(View.VISIBLE);
                        llInfo.setVisibility(View.GONE);
                        startRideTV.hide();

                        arrivalConfirmTV.hide();
                        break;
                    case Const.STATE_COMPLETED:
                        baseActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new HomeFragment())
                                .commit();
                        break;


                }
                initilizeMap();
                setUIData();

            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("arrived")) {
            if (status) {
                getRideDetail(rideDetails.id);
                startRideTV.show();
                arrivalConfirmTV.hide();
            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("started")) {
            if (status) {
                getRideDetail(jsonObject.optJSONObject("detail").optInt("id"));
            } else {
                gotoHomeFragment();
            }
        } else if (controller.equals("ride") && action.equals("completed")) {
            if (status)
                getRideDetail(jsonObject.optJSONObject("detail").optInt("id"));
            baseActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new HomeFragment())
                    .commit();
        } else {
            gotoHomeFragment();
        }


    }

    @Override
    public void onResume() {
        getRideDetail(rideId);
        super.onResume();
    }

    @SuppressLint("SetTextI18n")
    private void setUIData() {
        rideId = rideDetails.id;
        pickUpLocationTV.setText(rideDetails.location);
        tvDestination.setText(rideDetails.destination);
        passengerNameTV.setText(rideDetails.passengerDetail.first_name + " " + rideDetails.passengerDetail.last_name);
        if (rideDetails.passengerDetail.image_file != null && !rideDetails.passengerDetail.image_file.isEmpty()) {
            try {
                baseActivity.picasso.load(rideDetails.passengerDetail.image_file).error(R.mipmap.default_avatar).into(passengerProfileCIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String line) {
        setDestinationMarker(destination, time, line);
        if (rideDetails != null && rideDetails.state_id == Const.STATE_STARTED) {
            setDriverMarker(origin);
        } else
            setPickupMarker(origin);

    }

    private void call(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            baseActivity.showToast(String.valueOf(e));
        }

    }

    private void message(String number) {
        Intent message = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + number));
        message.putExtra("sms_body", getString(R.string.type_message_here));

        try {
            startActivity(message);
        } catch (Exception e) {
            showToast(getString(R.string.no_app_found_to_perform));
        }
    }


    @Override
    public void onLocationReceived(Location location) {
        currentLocation = location;
        initilizeMap();
        Util.log(TAG,"onLocationReceived Latitude: "+location.getLatitude()+" Longitude: "+location.getLongitude());
    }


    private void setCurrentLocation() {
        if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, SETUP_MAP_LOCATION_PERM_REQ, this)) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            currentLocation = location;
                            if (currentLocation != null) {
                                onLocationReceived(currentLocation);
                            } else {
                                showToast(getString(R.string.couldnt_get_current_location));
                            }
                        }
                    });

        } else showToast(getString(R.string.cnt_get_current_location));

    }


    @Override
    public void permGranted(int resultCode) {
        if (resultCode == SETUP_MAP_LOCATION_PERM_REQ)
            setCurrentLocation();
    }

    @Override
    public void permDenied(int resultCode) {

    }
}


